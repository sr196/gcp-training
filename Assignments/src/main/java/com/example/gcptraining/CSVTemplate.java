package com.example.gcptraining;
import org.apache.beam.runners.dataflow.DataflowRunner;
import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.io.TextIO;
import org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO;
import org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO.Write.CreateDisposition;
import org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO.Write.WriteDisposition;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.transforms.DoFn;
import org.apache.beam.sdk.transforms.ParDo;
import org.apache.beam.sdk.values.PCollection;
import com.google.api.services.bigquery.model.TableRow;

public class CSVTemplate {
	  @SuppressWarnings("serial")
	public static void main(String[] args) {
		RuntimeOptions options = PipelineOptionsFactory.as(RuntimeOptions.class);
		options.setTemplateLocation("gs://com_example_gcptraining/Templates/CSVTemplate");
		options.setJobName("bigquerywrite");
		options.setProject("gcp-training-246913");
		options.setTempLocation("gs://com_example_gcptraining/assignment_1/temp");
		options.setRunner(DataflowRunner.class);

	    Pipeline p = Pipeline.create(options);
	    
	    //Extract
	    PCollection<String> input = p.apply("Read Text data from Google Storage",TextIO.read().from(options.getInputFile())); 
	    
	    //Transform
	    PCollection<TableRow> output = input.apply("Text Data to Object",ParDo.of(new DoFn<String, TableRow>() {
	      @ProcessElement
	      public void processElement(ProcessContext c)  {
	    	String[] tokens = c.element().split(",");
	    	c.output(new TableRow()
	    			.set("id",tokens[0])
					.set("emp_name",tokens[1])
					.set("designation",tokens[2])
					.set("dob",tokens[3])
					.set("contact_no",tokens[4])
					.set("location",tokens[5])
					.set("salary",tokens[6]));
	      }
	    }));
	    
	    //Load
	    output.apply("Write to BigQuery Table",BigQueryIO.writeTableRows()
	            .to(options.getTableID())
	            .withCreateDisposition(CreateDisposition.CREATE_NEVER)
	            .withWriteDisposition(WriteDisposition.WRITE_TRUNCATE));
	    
	    p.run();
	    
	  }
}
