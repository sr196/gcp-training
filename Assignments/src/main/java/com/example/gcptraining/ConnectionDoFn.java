package com.example.gcptraining;

import org.apache.beam.sdk.transforms.DoFn;

@SuppressWarnings("serial")
public class ConnectionDoFn extends DoFn<String, String> {

	String inputFileName;

	public ConnectionDoFn(String inputFileName) {
		this.inputFileName = inputFileName;
	}
	
	public ConnectionDoFn() {}
}
