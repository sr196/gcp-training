package com.example.gcptraining;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Employee implements Serializable{
	private  int id;
	private  String emp_name;
	private  String designation;
	private  String dob;
	private  long contact_no;
	private  String location;
	private  double salary;

	public int getId() {
		return id;
	}
	public void setId(int id) {
		this.id = id;
	}
	public String getEmp_name() {
		return emp_name;
	}
	public void setEmp_name(String emp_name) {
		this.emp_name = emp_name;
	}
	public String getDesignation() {
		return designation;
	}
	public void setDesignation(String designation) {
		this.designation = designation;
	}
	public String getDob() {
		return dob;
	}
	public void setDob(String dob) {
		this.dob = dob;
	}
	public long getContact_no() {
		return contact_no;
	}
	public void setContact_no(long contact_no) {
		this.contact_no = contact_no;
	}
	public String getLocation() {
		return location;
	}
	public void setLocation(String location) {
		this.location = location;
	}
	public double getSalary() {
		return salary;
	}
	public void setSalary(double salary) {
		this.salary = salary;
	}

	@Override
	public String toString() {
		return "Employee [id=" + id + ", emp_name=" + emp_name + ", designation=" + designation + ", dob=" + dob
				+ ", contact_no=" + contact_no + ", location=" + location + ", salary=" + salary + "]";
	}
	
	
}
