package com.example.gcptraining;

import org.apache.beam.runners.dataflow.DataflowRunner;
import org.apache.beam.runners.dataflow.options.DataflowPipelineOptions;
import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.io.TextIO;
import org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO;
import org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO.Write.CreateDisposition;
import org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO.Write.WriteDisposition;
import org.apache.beam.sdk.options.PipelineOptions;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.transforms.DoFn;
import org.apache.beam.sdk.transforms.ParDo;
import org.apache.beam.sdk.transforms.SerializableFunction;
import org.apache.beam.sdk.values.PCollection;
import com.google.api.services.bigquery.model.TableRow;

public class BigQueryWrite {
 @SuppressWarnings("serial")
	public static void main(String[] args) {
		PipelineOptions options = PipelineOptionsFactory.create();
		options.setTempLocation("gs://com_example_gcptraining/assignment_1/temp");
	    DataflowPipelineOptions op = PipelineOptionsFactory.as(DataflowPipelineOptions.class);
	    op.setJobName("bigquerywrite");
	    op.setProject("gcp-training-246913");
	    op.setTempLocation("gs://com_example_gcptraining/assignment_1/temp");
	    op.setRunner(DataflowRunner.class);
	    String tableSpec = "gcp-training-246913:com_example_gcptraining.employee";
	    Pipeline p = Pipeline.create(options);

	    //Extract
	    PCollection<String> input = p.apply("Read Text data from Google Storage",TextIO.read().from("gs://com_example_gcptraining/assignment_1/emp_details.txt"));
	  
	    //Transform
	    PCollection<Employee> output = input.apply("Text Data to Object",ParDo.of(new DoFn<String, Employee>() {
	      @ProcessElement
	      public void processElement(ProcessContext c)  {
	    	String[] tokens = c.element().split(",");
	    	Employee emp = new Employee();
	    	emp.setId(Integer.parseInt(tokens[0]));
	    	emp.setEmp_name(tokens[1]);
	    	emp.setDesignation(tokens[2]);
	    	emp.setDob(tokens[3]);
	    	emp.setContact_no(Long.parseLong(tokens[4]));
	    	emp.setLocation(tokens[5]);
	    	emp.setSalary(Double.parseDouble(tokens[6]));
	    	c.output(emp);
	      }
	    }));

	    //Load
	    output.apply(BigQueryIO.<Employee>write()
	    		.to(tableSpec)
	    		.withCreateDisposition(CreateDisposition.CREATE_NEVER)
	    		.withWriteDisposition(WriteDisposition.WRITE_TRUNCATE)
	    		.withFormatFunction(new SerializableFunction<Employee, TableRow>() {

					@Override
					public TableRow apply(Employee emp) {
						return(new TableRow()
			    				.set("id",emp.getId())
			    				.set("emp_name",emp.getEmp_name())
			    				.set("designation",emp.getDesignation())
			    				.set("dob",emp.getDob())
			    				.set("contact_no",emp.getContact_no())
			    				.set("location",emp.getLocation())
			    				.set("salary",emp.getSalary()));
					}
	    			
				}));
	    p.run();
	    
	  }
}
