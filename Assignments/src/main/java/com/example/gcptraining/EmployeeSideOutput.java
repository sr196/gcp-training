package com.example.gcptraining;

import org.apache.beam.runners.dataflow.DataflowRunner;
import org.apache.beam.runners.dataflow.options.DataflowPipelineOptions;
import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.coders.StringUtf8Coder;
import org.apache.beam.sdk.io.TextIO;
import org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO;
import org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO.Write.CreateDisposition;
import org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO.Write.WriteDisposition;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.transforms.DoFn;
import org.apache.beam.sdk.transforms.ParDo;
import org.apache.beam.sdk.transforms.SerializableFunction;
import org.apache.beam.sdk.values.PCollection;
import org.apache.beam.sdk.values.PCollectionTuple;
import org.apache.beam.sdk.values.TupleTag;
import org.apache.beam.sdk.values.TupleTagList;

import com.google.api.services.bigquery.model.TableRow;

public class EmployeeSideOutput {

	@SuppressWarnings("serial")
	public static void main(String[] args) {
		DataflowPipelineOptions options = PipelineOptionsFactory.as(DataflowPipelineOptions.class);
		options.setJobName("empsideoutput");
		options.setProject("gcp-training-246913");
		options.setTempLocation("gs://com_example_gcptraining/assignment_1/temp");
		options.setRunner(DataflowRunner.class);
		
		Pipeline p = Pipeline.create(options);
		 TupleTag<String> validRecords = new TupleTag<String>();
		 TupleTag<String> inValidRecords = new TupleTag<String>();
		 
		 PCollection<String> inputFile = p.apply(TextIO.read().from("gs://com_example_gcptraining/assignment_3/emp_details"));
		 
		 PCollectionTuple records = inputFile.apply(ParDo.of(new DoFn<String, String>() {
			 @ProcessElement
			 public void processElement(ProcessContext c) {
				 String[] tokens = c.element().split(",");
				 if(tokens.length != 7)
					 c.output(inValidRecords, c.element());
				 else
					 c.output(validRecords,c.element());
			 }
		 })
				 .withOutputTags(validRecords, TupleTagList.of(inValidRecords)));
		 
		 records.get(validRecords).apply(BigQueryIO.<String>write().withFormatFunction(new SerializableFunction<String, TableRow>() {
			
			@Override
			public TableRow apply(String input) {
				String[] tokens = input.split(",");
				return(new TableRow()
						.set("id",tokens[0])
						.set("emp_name",tokens[1])
						.set("designation",tokens[2])
						.set("dob",tokens[3])
						.set("contact_no",tokens[4])
						.set("location",tokens[5])
						.set("salary",tokens[6]));
			}
		}).to("gcp-training-246913:com_example_gcptraining.emp")
				 .withCreateDisposition(CreateDisposition.CREATE_NEVER)
				 .withWriteDisposition(WriteDisposition.WRITE_TRUNCATE));
		 
		PCollection<String>inValid =  records.get(inValidRecords);
		inValid.setCoder(StringUtf8Coder.of()).apply(TextIO.write().to("gs://com_example_gcptraining/assignment_3/invalid_emp_details.txt").withoutSharding());
		 p.run();
	}

}
