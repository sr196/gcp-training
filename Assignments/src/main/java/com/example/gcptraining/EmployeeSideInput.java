package com.example.gcptraining;

import org.apache.beam.runners.dataflow.DataflowRunner;
import org.apache.beam.runners.dataflow.options.DataflowPipelineOptions;
import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.io.TextIO;
import org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO;
import org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO.Write.CreateDisposition;
import org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO.Write.WriteDisposition;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.transforms.Create;
import org.apache.beam.sdk.transforms.DoFn;
import org.apache.beam.sdk.transforms.ParDo;
import org.apache.beam.sdk.transforms.SerializableFunction;
import org.apache.beam.sdk.transforms.View;
import org.apache.beam.sdk.values.PCollection;
import org.apache.beam.sdk.values.PCollectionView;

import com.google.api.services.bigquery.model.TableRow;

public class EmployeeSideInput {

	@SuppressWarnings("serial")
	public static void main(String[] args) {
		DataflowPipelineOptions options = PipelineOptionsFactory.as(DataflowPipelineOptions.class);
		options.setJobName("empsideinput");
		options.setProject("gcp-training-246913");
		options.setTempLocation("gs://com_example_gcptraining/assignment_1/temp");
		options.setRunner(DataflowRunner.class);
		
		Pipeline p = Pipeline.create(options);
		PCollection<String> input = p.apply(TextIO.read().from("gs://com_example_gcptraining/assignment_2/flatten.txt"));
		PCollection<Integer> wordLength = p.apply(Create.of(6));
			
		PCollectionView<Integer> maxWorDCount = wordLength.apply(View.asSingleton());
		
		PCollection<String> validLengthWords = input.apply(ParDo.of(new DoFn<String, String>() {
			@ProcessElement
			public void processElement(ProcessContext c) {
				int len = c.sideInput(maxWorDCount);
				if(c.element().length()>=len) {
					c.output(c.element());
				}
			}
		}).withSideInputs(maxWorDCount));
		validLengthWords.apply(BigQueryIO.<String>write().withFormatFunction(new SerializableFunction<String, TableRow>() {
					
					@Override
					public TableRow apply(String input) {
						return (new TableRow().set("name", input));
					}
				})
				.to("gcp-training-246913:com_example_gcptraining.side_input_table")
				.withCreateDisposition(CreateDisposition.CREATE_NEVER)
				.withWriteDisposition(WriteDisposition.WRITE_APPEND));
		p.run();
	}

}
