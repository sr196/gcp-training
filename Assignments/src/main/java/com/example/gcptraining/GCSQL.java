package com.example.gcptraining;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.ResultSetMetaData;
import java.sql.SQLException;
import java.util.List;

import org.apache.beam.runners.dataflow.DataflowRunner;
import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.coders.BigEndianIntegerCoder;
import org.apache.beam.sdk.coders.KvCoder;
import org.apache.beam.sdk.coders.StringUtf8Coder;
import org.apache.beam.sdk.io.TextIO;
import org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO;
import org.apache.beam.sdk.io.gcp.bigquery.DynamicDestinations;
import org.apache.beam.sdk.io.gcp.bigquery.TableDestination;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.options.ValueProvider;
import org.apache.beam.sdk.transforms.Create;
import org.apache.beam.sdk.transforms.DoFn;
import org.apache.beam.sdk.transforms.ParDo;
import org.apache.beam.sdk.transforms.View;
import org.apache.beam.sdk.values.KV;
import org.apache.beam.sdk.values.PCollection;
import org.apache.beam.sdk.values.PCollectionView;
import org.apache.beam.sdk.values.ValueInSingleWindow;

import com.google.api.services.bigquery.model.TableRow;
import com.google.api.services.bigquery.model.TableSchema;
import com.google.common.collect.ImmutableList;


public class GCSQL {

      public static void main(String[] args) {
            RuntimeOptions options = PipelineOptionsFactory.as(RuntimeOptions.class);
            options.setJobName("gcp-training1");
            options.setTemplateLocation("gs://assignment_test_1/assignment5/templateSQLJDBC");
            options.setRunner(DataflowRunner.class);
            options.setProject("gcp-training-246913");
            options.setWorkerMachineType("n1-standard-4");

            Pipeline p = Pipeline.create(options);

            //Read Input
                          PCollection<String> input = p.apply("read input",TextIO.read().from(options.getInputFile()));
            PCollection<Integer> loop = p.apply(Create.of(0));
                          
                          //PCollection<String> fileView = p.apply(Create.ofProvider(options.getInput(),StringUtf8Coder.of()));
                          /*//Insert into Table
                          PCollection<String> seperateField= inputLine.apply(Regex.split(","));*/
                          
                          //PCollection<String> inputLine= input.apply(Regex.split("/n"));
                          
                          PCollection<TableRow> tblValues=input
                             .apply("Convert to table row",ParDo.of(new DoFn<String, TableRow>(){
                                    @ProcessElement
                                public void processElement(ProcessContext c) {
                                    String[] field = c.element().split(",");
                                    c.output(new TableRow()
                                    .set("id",field[0])
                                    .set("name", field[1])
                                    .set("department", field[2])
                                    .set("salary", field[3]));
                                 }
                          }));



            //TableReference tableRef = new TableReference();
            PCollection<String> dest=  loop.apply(ParDo.of(new DoFn<Integer,String>(){
                  ValueProvider<String> ip = options.getInputFile();
                  @ProcessElement
                  public void ProcessElement(ProcessContext c) {
                        Connection connection;
                        String tblName = null;
                        try {
                              connection = connectToCloudSql("jdbc:mysql://google/tst?cloudSqlInstance=gcp-training-246913:us-central1:test123&socketFactory=com.google.cloud.sql.mysql.SocketFactory","root","test123");

                              if(null == connection) {
                                    System.out.println("no connection");
                              }
                              else {
                                    //java.sql.Statement statement=connection.createStatement();
                                    String sql="select taleDestination from config where filename = ? ";
                                    java.sql.PreparedStatement prepareStatement = connection.prepareStatement(sql);
//                                  System.out.println("filename: "+ip.get());
                                    prepareStatement.setString(1, ip.get());
                                    prepareStatement.execute();
                                    ResultSet result = prepareStatement.getResultSet();
                                    ResultSetMetaData rsmd = result.getMetaData();
                                    result.next();
                                    
                                    for(int i=1;i<=rsmd.getColumnCount();i++) {
                                          System.out.println(result.getString(i));
                                          c.output(result.getString(i));
                                    }
                                    
                                    
//                                  tblName = result.getString("taleDestination");
////                                System.out.println("table: "+tblName);
//                                  c.output(tblName);
                              }
                        } catch (SQLException e) {
                              // TODO Auto-generated catch block
                              e.printStackTrace();
                        }
                  }
            }));


            PCollectionView<String> output = dest
                        .apply(View.<String>asSingleton());
            
            
      
                        




                        tblValues.apply("Insert to table",BigQueryIO.writeTableRows()
                         .to(
                                     new DynamicDestinations<TableRow, String>() {
            
                                                @Override
                                                public String getDestination(ValueInSingleWindow<TableRow> element) {
                                                      return sideInput(output);
                                                }
                                                
                                                @Override
                                                public List<PCollectionView<?>> getSideInputs(){
                                                //java.util.List<PCollectionView<?>> list= new java.util.ArrayList<PCollectionView<?>>();
                                                //list.add(output);
                                                      return ImmutableList.of(output);
                                                      
                                                }
                                                
                                                @Override
                                                public TableSchema getSchema(String destination) {
                                                      // TODO Auto-generated method stub
                                                      return null;
                                                }
            
                                                @Override
                                                public TableDestination getTable(String destination) {
                                                      return (new TableDestination(destination,""));
                                                }
                                           
                                          }
                                    )
                         .withWriteDisposition(BigQueryIO.Write.WriteDisposition.WRITE_APPEND)
                         .withCreateDisposition(BigQueryIO.Write.CreateDisposition.CREATE_NEVER)
                         );



            p.run();

      }

      public static Connection connectToCloudSql(String url,String u,String p) throws SQLException
      {
            try{
                  return DriverManager.getConnection(url,u,p);
            }
            catch(SQLException s)
            {
                  System.out.println(String.format("SQL exception. Could not establish connection to Cloud SQL: %s", s.toString()));
                  return null;
            }
            catch(Exception e)
            {
                  System.out.println(String.format("Exception. Could not establish connection to Cloud SQL: %s", e.toString()));
                  return null;
            }
      }

}

