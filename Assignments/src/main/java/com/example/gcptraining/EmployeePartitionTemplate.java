package com.example.gcptraining;

import org.apache.beam.runners.dataflow.DataflowRunner;
import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.io.TextIO;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.transforms.DoFn;
import org.apache.beam.sdk.transforms.MapElements;
import org.apache.beam.sdk.transforms.ParDo;
import org.apache.beam.sdk.transforms.Partition;
import org.apache.beam.sdk.transforms.SimpleFunction;
import org.apache.beam.sdk.transforms.Partition.PartitionFn;
import org.apache.beam.sdk.values.PCollectionList;

public class EmployeePartitionTemplate {

	@SuppressWarnings("serial")
	public static void main(String[] args) {
		RuntimeOptions options = PipelineOptionsFactory.as(RuntimeOptions.class);
		options.setJobName("empparttemplate");
		options.setTemplateLocation("gs://com_example_gcptraining/Templates/EmployeePartitionTemplate");
		options.setProject("gcp-training-246913");
		options.setTempLocation("gs://com_example_gcptraining/assignment_1/temp");
		options.setRunner(DataflowRunner.class);
		Pipeline p = Pipeline.create(options);
		
		PCollectionList<Employee> emps = p.apply(TextIO.read().from(options.getInputFile()))
		.apply(ParDo.of(new DoFn<String, Employee>(){
			@ProcessElement
			public void processElement(ProcessContext c) {
				String[] tokens = c.element().split(",");
				Employee emp = new Employee();
				emp.setId(Integer.parseInt(tokens[0]));
				emp.setEmp_name(tokens[1]);
				emp.setDesignation(tokens[2]);
				emp.setDob(tokens[3]);
				emp.setContact_no(Long.parseLong(tokens[4]));
				emp.setLocation(tokens[5]);
				emp.setSalary(Double.parseDouble(tokens[6]));
				c.output(emp);
			}
		})).apply(Partition.of(3, new PartitionFn<Employee>() {

			@Override
			public int partitionFor(Employee emp, int numPartitions) {
				return (int)emp.getSalary()%3;
			}
		}));
		
		emps.get(0).apply(MapElements.via(new SimpleFunction<Employee, String>() {
			@Override
			public String apply(Employee emp) {
				return (emp.getId()+","
						+emp.getEmp_name()+","
						+emp.getDesignation()+","
						+emp.getDob()+","
						+emp.getContact_no()+","
						+emp.getLocation()+","
						+emp.getSalary());
			}
		})).apply(TextIO.write().to("gs://com_example_gcptraining/assignment_2/partOne.txt").withoutSharding());
		emps.get(1).apply(MapElements.via(new SimpleFunction<Employee, String>() {
			@Override
			public String apply(Employee emp) {
				return (emp.getId()+","
						+emp.getEmp_name()+","
						+emp.getDesignation()+","
						+emp.getDob()+","
						+emp.getContact_no()+","
						+emp.getLocation()+","
						+emp.getSalary());
			}
		})).apply(TextIO.write().to("gs://com_example_gcptraining/assignment_2/partTwo.txt").withoutSharding());
		emps.get(2).apply(MapElements.via(new SimpleFunction<Employee, String>() {
			@Override
			public String apply(Employee emp) {
				return (emp.getId()+","
						+emp.getEmp_name()+","
						+emp.getDesignation()+","
						+emp.getDob()+","
						+emp.getContact_no()+","
						+emp.getLocation()+","
						+emp.getSalary());
			}
		})).apply(TextIO.write().to("gs://com_example_gcptraining/assignment_2/partThree.txt").withoutSharding());
		
		p.run();

	}

}
