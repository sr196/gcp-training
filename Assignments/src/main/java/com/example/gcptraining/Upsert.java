package com.example.gcptraining;

import org.apache.beam.runners.dataflow.options.DataflowPipelineOptions;
import org.apache.beam.runners.direct.DirectRunner;
import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.coders.AvroCoder;
import org.apache.beam.sdk.coders.KvCoder;
import org.apache.beam.sdk.extensions.joinlibrary.Join;
import org.apache.beam.sdk.io.TextIO;
import org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO;
import org.apache.beam.sdk.io.gcp.bigquery.SchemaAndRecord;
import org.apache.beam.sdk.io.gcp.bigquery.TableRowJsonCoder;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.transforms.DoFn;
import org.apache.beam.sdk.transforms.ParDo;
import org.apache.beam.sdk.transforms.SerializableFunction;
import org.apache.beam.sdk.values.KV;
import org.apache.beam.sdk.values.PCollection;
import org.apache.beam.sdk.values.TupleTag;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.api.services.bigquery.model.TableRow;

public class Upsert {
	public static Logger LOG = LoggerFactory.getLogger(Logger.class);

	@SuppressWarnings("serial")
	public static void main(String[] args) {
		DataflowPipelineOptions options = PipelineOptionsFactory.as(DataflowPipelineOptions.class);
		options.setJobName("left-join");
		options.setProject("gcp-training-246913");
		options.setTempLocation("gs://com_example_gcptraining/temp");
		options.setRunner(DirectRunner.class);
		
		Pipeline p = Pipeline.create(options);
		TupleTag<TableRow> inputTag = new TupleTag<TableRow>();
		TupleTag<TableRow> tableTag = new TupleTag<TableRow>();
		PCollection<KV<String, TableRow>> inputFileKV = p.apply("Read Input File",TextIO.read().from("gs://com_example_gcptraining/assignment_1/emp_details.txt"))
		.apply("Create Input KV",ParDo.of(new DoFn<String, KV<String, TableRow>>(){
			@ProcessElement
			public void processElement(ProcessContext c) {
				String[] tokens = c.element().split(",");
				c.output(KV.of(tokens[0], new TableRow()
						.set("id", tokens[0])
						.set("emp_name", tokens[1])
						.set("designation", tokens[2])
						.set("dob", tokens[3])
						.set("contact_no", tokens[4])
						.set("location", tokens[5])
						.set("salary", tokens[6])));
			}
		}));
		PCollection<KV<String, TableRow>> bigqueryTableKV = p.apply("Read BigQuery Table",BigQueryIO.read(new SerializableFunction<SchemaAndRecord, KV<String, TableRow>>() {

			@Override
			public KV<String, TableRow> apply(SchemaAndRecord input) {
				return KV.of(String.valueOf(input.getRecord().get("id")),new TableRow()
						.set("id",input.getRecord().get("id"))
						.set("emp_name",input.getRecord().get("emp_name"))
						.set("designation",input.getRecord().get("designation"))
						.set("dob",input.getRecord().get("dob"))
						.set("contact_no",input.getRecord().get("contact_no"))
						.set("location",input.getRecord().get("location"))
						.set("salary",input.getRecord().get("salary")));
			}
		}).from("gcp-training-246913:com_example_gcptraining.emp")
				.withCoder(KvCoder.of(AvroCoder.of(String.class),TableRowJsonCoder.of())));
		
//		PCollection<KV<String, KV<TableRow, TableRow>>> joinResult = 
				Join.leftOuterJoin(inputFileKV, bigqueryTableKV, new TableRow())
				.apply("Reading From joinResult",ParDo.of(new DoFn<KV<String, KV<TableRow, TableRow>>,Void>(){
					@ProcessElement
					public void processElement(ProcessContext c) {
						LOG.info(c.element().getKey() + 
								c.element().getValue().getValue().getF()
								);
					}
				}));
		
		
//		KeyedPCollectionTuple.of(inputTag, inputFileKV).and(tableTag, bigqueryTableKV).apply("Applying CoGroupByKey",CoGroupByKey.<String>create())
//				.apply("Upsert Table",ParDo.of(new DoFn<KV<String, CoGbkResult>, TableRow> () {
//					@ProcessElement
//					public void processElement(ProcessContext c) {
//						KV<String, CoGbkResult> element = c.element();
//						for(TableRow leftRow : element.getValue().getAll(inputTag)) {
//							if(leftRow == null)
//								break;
//							for(TableRow rightRow : element.getValue().getAll(tableTag)) {
//
//								if(leftRow.get("id")!=rightRow.get("id"))
//									c.output(leftRow);
//								c.output(leftRow);
//							}
//						}
//					}
//				})).apply("Update Table",BigQueryIO.writeTableRows()
//						.to("gcp-training-246913:com_example_gcptraining.emp")
//						.withCreateDisposition(CreateDisposition.CREATE_NEVER)
//						.withWriteDisposition(WriteDisposition.WRITE_APPEND)); 
		p.run();
	}

}
