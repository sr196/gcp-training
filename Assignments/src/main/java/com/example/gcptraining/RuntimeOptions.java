package com.example.gcptraining;

import org.apache.beam.runners.dataflow.options.DataflowPipelineOptions;
import org.apache.beam.sdk.options.ValueProvider;

public interface RuntimeOptions extends DataflowPipelineOptions{
	ValueProvider<String> getInputFile();
	ValueProvider<String> getTableID();
	void setInputFile(ValueProvider<String> value);
	void setTableID(ValueProvider<String> value);
}
