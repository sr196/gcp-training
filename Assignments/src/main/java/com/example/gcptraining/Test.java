package com.example.gcptraining;

import java.util.ArrayList;
import java.util.List;

import org.apache.beam.runners.dataflow.options.DataflowPipelineOptions;
import org.apache.beam.runners.direct.DirectRunner;
import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.io.TextIO;
import org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO;
import org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO.Write.CreateDisposition;
import org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO.Write.WriteDisposition;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.transforms.Create;
import org.apache.beam.sdk.transforms.DoFn;
import org.apache.beam.sdk.transforms.ParDo;
import org.apache.beam.sdk.values.PCollection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.api.services.bigquery.model.TableCell;
import com.google.api.services.bigquery.model.TableRow;

public class Test {
	public static Logger LOG = LoggerFactory.getLogger(Logger.class);

	@SuppressWarnings("serial")
	public static void main(String[] args) {
		DataflowPipelineOptions options = PipelineOptionsFactory.as(DataflowPipelineOptions.class);
		options.setJobName("testtest");
		options.setTempLocation("gs://com_example_gcptraining/TempLocation");
		options.setProject("gcp-training-246913");
		options.setRunner(DirectRunner.class);
		Pipeline p = Pipeline.create(options);
		
		p.apply(TextIO.read().from("gs://com_example_gcptraining/PubSub/gsapus_tds_tran.csv.json"))
				.apply(ParDo.of(new DoFn<String, TableRow>(){
					@ProcessElement
					public void processElement(ProcessContext c) {
						String element = c.element();
						element = element.replace("{", "");
						element = element.replace("}", "");
						String[] tokens = element.split(",");
						List<TableCell> cellList = new ArrayList<TableCell>();
						int count = 1;
						for(String token : tokens) {
							String output;
							token=token.replaceFirst(" ", "");
							String[] fields = token.split(":");
							if(fields.length==1) {
								cellList.add(new TableCell().setV(""));
								count++;
							} else {
								cellList.add(new TableCell().setV( fields[1].replace("\"", "")));
								count++;
							}
								
						}
						LOG.info("!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!!"+count);
						c.output(new TableRow().setF(cellList));
					}
				}))
				.apply(BigQueryIO.writeTableRows()
						.to("gcp-training-246913:com_example_gcptraining.pubsub")
						.withCreateDisposition(CreateDisposition.CREATE_NEVER)
						.withWriteDisposition(WriteDisposition.WRITE_TRUNCATE));
		
	
		
		p.run();
	}

}
