/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package com.example.gcptraining;

import java.util.StringTokenizer;

import org.apache.beam.runners.dataflow.DataflowRunner;
import org.apache.beam.runners.dataflow.options.DataflowPipelineOptions;
import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.coders.Coder;
import org.apache.beam.sdk.io.TextIO;
import org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO;
import org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO.Write;
import org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO.Write.CreateDisposition;
import org.apache.beam.sdk.io.gcp.bigquery.BigQueryIO.Write.WriteDisposition;
import org.apache.beam.sdk.options.PipelineOptions;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.transforms.Create;
import org.apache.beam.sdk.transforms.DoFn;
import org.apache.beam.sdk.transforms.MapElements;
import org.apache.beam.sdk.transforms.ParDo;
import org.apache.beam.sdk.transforms.SerializableFunction;
import org.apache.beam.sdk.transforms.SimpleFunction;
import org.apache.beam.sdk.values.PCollection;
import org.apache.beam.sdk.values.TypeDescriptor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import java.time.LocalDate;
import com.google.api.services.bigquery.Bigquery;
import com.google.api.services.bigquery.model.Table;
import com.google.api.services.bigquery.model.TableFieldSchema;
import com.google.api.services.bigquery.model.TableRow;
import com.google.api.services.bigquery.model.TableSchema;
import com.google.common.collect.ImmutableList;


public class StarterPipeline {
  private static final Logger LOG = LoggerFactory.getLogger(StarterPipeline.class);

  @SuppressWarnings("serial")
public static void main(String[] args) {
	PipelineOptions options = PipelineOptionsFactory.create();
	options.setTempLocation("gs://com_example_gcptraining/assignment_1/temp");
    DataflowPipelineOptions op = PipelineOptionsFactory.as(DataflowPipelineOptions.class);

    op.setJobName("bigquerywrite");
    op.setProject("gcp-training-246913");
    op.setTempLocation("gs://com_example_gcptraining/assignment_1/temp");
    op.setRunner(DataflowRunner.class);
    Pipeline p = Pipeline.create(op);

    
    PCollection<String> input = p.apply("Read Text data from Google Storage",TextIO.read().from("gs://com_example_gcptraining/assignment_1/emp_details.txt"));
    PCollection<Employee> output = input.apply("Text Data to Object",ParDo.of(new DoFn<String, Employee>() {
      @ProcessElement
      public void processElement(ProcessContext c)  {
    	String[] tokens = c.element().split(",");
    	Employee emp = new Employee();
    	emp.setId(Integer.parseInt(tokens[0]));
    	emp.setEmp_name(tokens[1]);
    	emp.setDesignation(tokens[2]);
    	emp.setDob(tokens[3]);
    	emp.setContact_no(Long.parseLong(tokens[4]));
    	emp.setLocation(tokens[5]);
    	emp.setSalary(Double.parseDouble(tokens[6]));
    	c.output(emp);
      }
    }));
	
	TableSchema tableSchema = new TableSchema()
    		.setFields(
    				ImmutableList.of(
    						new TableFieldSchema()
    						.setName("id")
    						.setType("Integer")
    						.setMode("Required"),
    						new TableFieldSchema()
    						.setName("emp_name")
    						.setType("String")
    						.setMode("Nullable"),
    						new TableFieldSchema()
    						.setName("designation")
    						.setType("String")
    						.setMode("Nullable"),
    						new TableFieldSchema()
    						.setName("dob")
    						.setType("Date")
    						.setMode("Nullable"),
    						new TableFieldSchema()
    						.setName("contact_no")
    						.setType("Numeric")
    						.setMode("Nullable"),
    						new TableFieldSchema()
    						.setName("location")
    						.setType("String")
    						.setMode("Nullable"),
    						new TableFieldSchema()
    						.setName("salary")
    						.setType("float")
    						.setMode("Nullable")
    				));

    
    String tableSpec = "gcp-training-246913:com_example_gcptraining.employee";
    output.apply("Create BigQuery Record",MapElements.into(TypeDescriptor.of(TableRow.class))
    		.via((Employee emp) -> new TableRow()
    				.set("id",emp.getId())
    				.set("emp_name",emp.getEmp_name())
    				.set("designation",emp.getDesignation())
    				.set("dob",emp.getDob())
    				.set("contact_no",emp.getContact_no())
    				.set("location",emp.getLocation())
    				.set("salary",emp.getSalary())))
    .apply("Write to BigQuery Table",BigQueryIO.writeTableRows()
            .to(tableSpec)
            .withSchema(tableSchema)
            .withCreateDisposition(CreateDisposition.CREATE_IF_NEEDED)
            .withWriteDisposition(WriteDisposition.WRITE_TRUNCATE));

    p.run();
    
  }
}
