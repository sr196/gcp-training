package com.example.gcptraining;

import org.apache.beam.sdk.Pipeline;
import org.apache.beam.sdk.io.TextIO;
import org.apache.beam.sdk.options.PipelineOptions;
import org.apache.beam.sdk.options.PipelineOptionsFactory;
import org.apache.beam.sdk.transforms.Create;
import org.apache.beam.sdk.transforms.Flatten;
import org.apache.beam.sdk.values.PCollection;
import org.apache.beam.sdk.values.PCollectionList;

public class FlattenTransform {

	public static void main(String[] args) {
		PipelineOptions options = PipelineOptionsFactory.create();
		Pipeline p = Pipeline.create(options);
		PCollection<String> p1 = p.apply(Create.of("Shubham","Rohan"));
		PCollection<String> p2 = p.apply(Create.of("Nilesh","Ritwik"));
		PCollection<String> p3 = p.apply(Create.of("John","Doe"));
		
		PCollectionList<String> collections = PCollectionList.of(p1).and(p2).and(p3);
		collections.apply(Flatten.pCollections())
		.apply(TextIO.write().to("gs://com_example_gcptraining/assignment_2/flatten.txt").withoutSharding());
		p.run();
		}

}
